#!/bin/bash


# Exit when any command fails
set -e

# Install required dependencies
pip install -r requires/requires.txt
pip install -r requires/requires-test.txt
echo "Successfully installed required packages"

# Run tests
pytest astronaut_analysis_test.py --junitxml=pytest.xml
echo "Successfully ran tests checks"

# Check the code using the flake8 linter
ruff --select ALL --ignore PTH,T,PLR,ANN,D205 --output-file=ruff.json --output-format=gitlab astronaut_analysis.py
echo "Successfully ran ruff checks"

# Check that the script is basically working and creating the same results
python astronaut_analysis.py ../data/astronauts.json
test -f boxplot.png
test -f combined_histogram.png
test -f female_humans_in_space.png
test -f humans_in_space.png
test -f male_humans_in_space.png
echo "Successfully created the plots"
